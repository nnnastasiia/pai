create table transactions
(
    id_transaction bigserial
        constraint transactions_pk
            primary key,
    id_user        bigint             not null,
    id_order       bigint             not null
        constraint transactions_order___fk
            references "order" (id_order)
            on update cascade on delete cascade,
    code           varchar(100)       not null,
    type           smallint default 0 not null,
    mode           smallint default 0 not null,
    status         smallint default 0 not null,
    "createdAt"    date               not null,
    "updatedAt"    date               not null
);

alter table transactions
    owner to rceofbuiywkdal;

create unique index transactions_id_transaction_uindex
    on transactions (id_transaction);

