create table products_review
(
    id_product_review bigserial,
    id_product        bigint             not null
        constraint products_review_product___fk
            references products (id_product)
            on update cascade on delete cascade,
    raiting           smallint default 0 not null
);

alter table products_review
    owner to rceofbuiywkdal;

