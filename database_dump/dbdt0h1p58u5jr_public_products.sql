create table products
(
    id_product      bigserial,
    "nameOfProduct" varchar not null,
    description     text    not null,
    product_image   varchar not null,
    id_size         integer
        constraint size_of_products___fk
            references sizes
            on update cascade on delete cascade,
    price           bigint  not null,
    id_category     integer not null
        constraint category_of_products___fk
            references categories
            on update cascade on delete cascade
);

alter table products
    owner to rceofbuiywkdal;

create unique index products_id_product_uindex
    on products (id_product);

INSERT INTO public.products (id_product, "nameOfProduct", description, product_image, id_size, price, id_category) VALUES (15, 'Adamant', 'nterdum lectus quam duis ornare habitant dolor accumsan. Imperdiet dolor arcu.', 'uploads/7b71c9e39089cd9cb490f0da3ce3942a239fa5b0.png', null, 300, 19);
INSERT INTO public.products (id_product, "nameOfProduct", description, product_image, id_size, price, id_category) VALUES (16, 'Fall Romance', 'Elegance of roses, simplicity of Calla and Brightness of Sunflower combined in fall hues to embrace the season’s colors.', 'uploads/352c6e2ddcf16c6b3097e7bdd90825db8475e94e.png', null, 310, 19);
INSERT INTO public.products (id_product, "nameOfProduct", description, product_image, id_size, price, id_category) VALUES (17, 'Joyfull Fragrant', 'This unforgettable composition of Cymbidium orchid and cellosia will sooth the soul and cheer your heart! Gentle color scheme makes this bouquet a lovely present.', 'uploads/82ac12da6d64b3a8881c14db1f877f6b8f1a0a93.png', null, 320, 19);
INSERT INTO public.products (id_product, "nameOfProduct", description, product_image, id_size, price, id_category) VALUES (18, 'Purple Expression', 'Deeply-hued Purple blooms of Orchids, Dahlias and Roses are a symbol of modesty and faithfulness. Rustic arrangement with bright appearance in it''s natural beauty in vibrant shades.', 'uploads/06f7596363c92270ebb6f0c9d1761752a61b60ac.png', null, 340, 19);