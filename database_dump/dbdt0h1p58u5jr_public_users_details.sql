create table users_details
(
    "First_name" varchar(50) not null,
    "Surname"    varchar(70) not null,
    phone_number varchar(13) not null,
    city         varchar(30) not null,
    address      varchar(255),
    card_number  varchar(16),
    id_user      integer     not null
        constraint users_details_pk
            references users
            on update cascade on delete cascade
);

alter table users_details
    owner to rceofbuiywkdal;

create unique index users_details_card_number_uindex
    on users_details (card_number);

create unique index users_details_phone_number_uindex
    on users_details (phone_number);

INSERT INTO public.users_details ("First_name", "Surname", phone_number, city, address, card_number, id_user) VALUES ('Admin', 'Sklepu', '827 211 121', 'Kraków', null, null, 13);
INSERT INTO public.users_details ("First_name", "Surname", phone_number, city, address, card_number, id_user) VALUES ('Anastasiia', 'Mishchenko', '664 140 134', 'Kraków', null, null, 14);