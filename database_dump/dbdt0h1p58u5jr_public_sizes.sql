create table sizes
(
    id_size   serial
        constraint sizes_pk
            primary key,
    size      varchar not null,
    size_desc text
);

alter table sizes
    owner to rceofbuiywkdal;

create unique index sizes_id_size_uindex
    on sizes (id_size);

