create table cart_items
(
    id_cart_item bigserial,
    id_user      bigint             not null
        constraint cart_items_user_fk
            references users
            on update cascade on delete cascade,
    id_product   bigint             not null
        constraint cart_items_products___fk
            references products (id_product)
            on update cascade on delete cascade,
    "createdAt"  date default now() not null,
    "updatedAt"  date default now() not null
);

alter table cart_items
    owner to rceofbuiywkdal;

create unique index cart_items_id_cart_item_uindex
    on cart_items (id_cart_item);

INSERT INTO public.cart_items (id_cart_item, id_user, id_product, "createdAt", "updatedAt") VALUES (17, 13, 15, '2022-02-03', '2022-02-03');
INSERT INTO public.cart_items (id_cart_item, id_user, id_product, "createdAt", "updatedAt") VALUES (18, 13, 17, '2022-02-03', '2022-02-03');