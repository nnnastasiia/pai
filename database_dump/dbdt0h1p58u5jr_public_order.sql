create table "order"
(
    id_order     bigserial,
    id_user      integer                  not null
        constraint order_pk
            primary key
        constraint users_order___fk
            references users
            on update cascade on delete cascade,
    id_session   varchar(100)             not null,
    token        varchar(100)             not null,
    status       smallint       default 0 not null,
    total_price  numeric(10, 2) default 0 not null,
    "First_name" varchar(50)              not null,
    "Surname"    varchar(50)              not null,
    phone_number varchar(9)               not null,
    email        varchar(70)              not null,
    city         varchar(50)              not null,
    poscode      varchar(5)               not null,
    address      varchar(255)             not null,
    "createdAt"  date                     not null,
    "updatedAt"  date                     not null
);

alter table "order"
    owner to rceofbuiywkdal;

create unique index order_id_order_uindex
    on "order" (id_order);

create unique index order_phone_number_uindex
    on "order" (phone_number);

