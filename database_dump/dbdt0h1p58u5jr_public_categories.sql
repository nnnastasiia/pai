create table categories
(
    id_category             serial
        constraint categories_pk
            primary key,
    name_of_category        varchar                                                              not null,
    description_of_category text                                                                 not null,
    category_image_url      varchar default 'public/images/default_image.png'::character varying not null
);

alter table categories
    owner to rceofbuiywkdal;

create unique index categories_id_category_uindex
    on categories (id_category);

INSERT INTO public.categories (id_category, name_of_category, description_of_category, category_image_url) VALUES (19, 'Ślubna florystyka', 'Interdum lectus quam duis ornare habitant dolor accumsan. Imperdiet dolor arcu.', 'uploads/d352e9aea763c6948b8d251b92a2e194738664ce.png');
INSERT INTO public.categories (id_category, name_of_category, description_of_category, category_image_url) VALUES (20, 'Dekoracji', 'Interdum lectus quam duis ornare habitant dolor accumsan. Imperdiet dolor arcu.', 'uploads/f7b0db11e7b202acbe5cc02e040be0622d1a319e.png');
INSERT INTO public.categories (id_category, name_of_category, description_of_category, category_image_url) VALUES (21, 'Ogród', 'Interdum lectus quam duis ornare habitant dolor accumsan. Imperdiet dolor arcu.', 'uploads/dce9f76aac6d1eb8efc7c39f5aa3c2d139e78fe2.png');
INSERT INTO public.categories (id_category, name_of_category, description_of_category, category_image_url) VALUES (22, 'Święta', 'Interdum lectus quam duis ornare habitant dolor accumsan. Imperdiet dolor arcu.', 'uploads/0966eaca618f89baccdf2d54752dc380401bec08.png');