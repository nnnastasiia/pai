create table order_item
(
    id_order_item bigserial,
    id_product    bigint             not null
        constraint order_item_pk
            primary key
        constraint order_item_product___fk
            references products (id_product)
            on update cascade on delete cascade,
    id_order      bigint             not null
        constraint order_item_order___fk
            references "order" (id_order)
            on update cascade on delete cascade,
    price         numeric(10, 2)     not null,
    quantity      smallint default 0 not null,
    "createdAt"   date               not null,
    "updatedAt"   date               not null
);

alter table order_item
    owner to rceofbuiywkdal;

create unique index order_item_id_order_item_uindex
    on order_item (id_order_item);

