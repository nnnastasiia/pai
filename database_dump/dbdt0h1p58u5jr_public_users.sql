create table users
(
    id_user          bigserial
        constraint users_pk
            primary key,
    email            varchar(50)               not null,
    password         varchar(100)              not null,
    date_of_register date        default now() not null,
    role             varchar(10) default 'User'::character varying
);

alter table users
    owner to rceofbuiywkdal;

create unique index users_id_user_uindex
    on users (id_user);

INSERT INTO public.users (id_user, email, password, date_of_register, role) VALUES (13, 'admin_kwiaty@mail.com', 'd829b843a6550a947e82f2f38ed6b7a7', '2022-02-03', 'Admin');
INSERT INTO public.users (id_user, email, password, date_of_register, role) VALUES (14, 'nastka.mishchenko@gmail.com', 'c90b7f4378a55a9170642af29922cf5c', '2022-02-03', 'User');