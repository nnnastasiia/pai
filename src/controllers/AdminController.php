<?php

require_once 'AppController.php';

require_once __DIR__ . '/../services/UploadService.php';

require_once __DIR__ . '/../repository/CategoryRepository.php';
require_once __DIR__ . '/../repository/ProductRepository.php';


class AdminController extends AppController
{

    private function isAuthed(): bool
    {
        if ($this->authService->isAuthed()) {
            return true;
        }
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/login");
        return false;
    }

    private function isAdmin(): bool
    {
        if ($this->adminService->isAdmin()) {
            return true;
        }
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/mainpage");
        return false;
    }


    public function adminpanel()
    {
        if (!$this->isAuthed()) return;
        if (!$this->isAdmin()) return;

        $categoryRepository = new CategoryRepository();
        $productRepository = new ProductRepository();

        $categories = $categoryRepository->getAll();
        $products = $productRepository->getAll();

        if (!$this->isPost() || !isset($_POST["submit"])) {
            $this->render('adminpanel', [
                'categories' => $categories,
                'products' => $products
            ]);
            return;
        }

        $uploadService = new UploadService();

        switch ($_POST["submit"]) {
            case 'Create Category':
                $upload = $uploadService->uploadFile('categoryFile');
                if (!$upload['status']) {
                    break;
                }
                $name = $_POST['category_name'];
                $desc = $_POST['category_desc'];
                $uploadedCategory = $categoryRepository->addCategory($name, $desc, $upload['message']);
                $categories[] = $uploadedCategory;
                break;
            case 'Create Product':
                $upload = $uploadService->uploadFile('productFile');
                if (!$upload['status']) {
                    break;
                }
                $name = $_POST['product_name'];
                $desc = $_POST['product_desc'];
                $price = $_POST['product_price'];
                $catId = $_POST['product_category_id'];
                $uploadedProduct = $productRepository->addProduct($name, $desc, $catId, $price, $upload['message']);
                $products[] = $uploadedProduct;
                break;
        }


        $this->render('adminpanel', [
            'messages' => [$upload['status'] . ' - ' . $upload['message']],
            'categories' => $categories,
            'products' => $products
        ]);

    }

    public function deleteCategory()
    {
        if (!$this->isAuthed()) {
            http_response_code(401);
            return;
        }
        if (!$this->isAdmin()) {
            http_response_code(403);
            return;
        }

        $categoryRepository = new CategoryRepository();

        if (isset($_GET['categoryId']) && $categoryRepository->deleteCategory($_GET['categoryId'])) {
            http_response_code(200);
        } else {
            http_response_code(500);
        }
    }

    public function deleteProduct()
    {
        if (!$this->isAuthed()) {
            http_response_code(401);
            return;
        }
        if (!$this->isAdmin()) {
            http_response_code(403);
            return;
        }

        $productRepository = new ProductRepository();

        if (isset($_GET['productId']) && $productRepository->deleteProduct($_GET['productId'])) {
            http_response_code(200);
        } else {
            http_response_code(500);
        }
    }


}
