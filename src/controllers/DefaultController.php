<?php

require_once 'AppController.php';

require_once __DIR__ . '/../repository/CategoryRepository.php';
require_once __DIR__ . '/../repository/ProductRepository.php';
require_once __DIR__ . '/../repository/CartRepository.php';


class DefaultController extends AppController
{

    private function isAuthed(): bool
    {
        if ($this->authService->isAuthed()) {
            return true;
        }
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/login");
        return false;
    }


    public function mainpage()
    {
        // Can be entered without auth

        $categoryRepository = new CategoryRepository();
        $productRepository = new ProductRepository();

        $categories = $categoryRepository->getAll(4);
        $products = $productRepository->getAll(4);

        if ($this->authService->isAuthed()) {
            $cartRepository = new CartRepository();
            $cartProductsIds = $cartRepository->getCartProductsIds($this->authService->getUser()->getId());
        }

        $this->render('mainpage', [
            'categories' => $categories,
            'products' => $products,
            'cartProductsIds' => $cartProductsIds ?? []
        ]);
    }

    public function katalog()
    {
        // Can be entered without auth

        $productRepository = new ProductRepository();

        $products = $productRepository->getAll();

        if ($this->authService->isAuthed()) {
            $cartRepository = new CartRepository();
            $cartProductsIds = $cartRepository->getCartProductsIds($this->authService->getUser()->getId());
        }

        $this->render('katalog', [
            'products' => $products,
            'cartProductsIds' => $cartProductsIds ?? []
        ]);
    }

    public function productPage()
    {
        // Can be entered without auth

        if (!isset($_GET['id'])) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/katalog");
            return;
        }

        $productRepository = new ProductRepository();
        $product = $productRepository->getById($_GET['id']);

        if (!$product) {
            $this->render('productPage', ['messages' => ['Product not found!']]);
        }

        $this->render('productPage', ['product' => $product]);
    }

    public function shoppingCart()
    {
        if (!$this->isAuthed()) return;

        $cartRepository = new CartRepository();
        $cartItems = $cartRepository->getCartProducts($this->authService->getUser()->getId());


        $this->render('shoppingCart', ['cartItems' => $cartItems]);
    }

    public function payment()
    {
        if (!$this->isAuthed()) return;
        $this->render('payment');
    }

    public function userPage()
    {
        if (!$this->isAuthed()) return;
        $this->render('userPage');
    }


    /*          ENDPOINTS           */

    public function deleteCartItem()
    {
        if (!$this->isAuthed()) {
            http_response_code(401);
            return;
        }

        $cartRepository = new CartRepository();

        if (isset($_GET['itemId']) && $cartRepository->deleteProductFromCart($_GET['itemId'])) {
            http_response_code(200);
        } else {
            http_response_code(500);
        }
    }

    public function addToCart()
    {
        if (!$this->isAuthed()) {
            http_response_code(401);
            return;
        }

        $cartRepository = new CartRepository();

        if (isset($_GET['productId']) && $cartRepository->addProductToCart($this->authService->getUser()->getId(), $_GET['productId'])) {
            http_response_code(200);
        } else {
            http_response_code(500);
        }


    }


}
