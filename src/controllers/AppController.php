<?php

require_once __DIR__ .  '/../services/AuthService.php';
require_once __DIR__ .  '/../services/AdminService.php';

class AppController {

    private $request;
    protected $authService;
    protected $adminService;

    public function __construct()
    {
        $this->authService = new AuthService();
        $this->adminService = new AdminService();
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function isGet(): bool
    {
        return $this->request === 'GET';
    }

    protected function isPost(): bool
    {
        return $this->request === 'POST';
    }

    protected function render(string $template = null, array $variables = []) {

        $templatePath = 'public/views/pages/'.$template.'.php';

        $output = 'File not found';

        if(file_exists($templatePath)) {
            extract($variables);
            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }

        print $output;
    }


}
