<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../repository/UserRepository.php';

class SecurityController extends AppController
{

    private function isAuthed(): bool
    {
        if ($this->authService->isAuthed()) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/mainpage");
            return true;
        }
        return false;
    }

    public function login()
    {

        if ($this->isAuthed()) return;

        $userRepository = new UserRepository();


        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $user = $userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['User not found!']]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        }

        if ($user->getPassword() !== $password) {
            return $this->render('login', ['messages' => ['Wrong password!']]);
        }

        $_SESSION["user"] = serialize($user);

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/mainpage");

    }


    public function registration()
    {
        if ($this->isAuthed()) return;

        if (!$this->isPost()) {
            return $this->render('registration');;
        }

        $userRepository = new UserRepository();
        $userId = $userRepository->registerUser(
            $_POST['email'],
            md5($_POST['password']),
            $_POST['firstname'],
            $_POST['surname'],
            $_POST['phone_number'],
            $_POST['city']
        );

        if (!$userId) {
            return $this->render('registration', ['messages' => ['Wrong registration data!']]);
        }

        $user = new User(
            $userId,
            $_POST['email'],
            md5($_POST['password']),
            $_POST['firstname'],
            $_POST['surname'],
            $_POST['phone_number'],
            $_POST['city'],
            'User'
        );

        $_SESSION["user"] = serialize($user);

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/mainpage");

    }

    public function logout()
    {
        session_start();
        session_destroy();
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/mainpage");
    }


}
