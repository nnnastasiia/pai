<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/User.php';

class UserRepository extends Repository
{

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users u 
                JOIN public.users_details d ON u.id_user = d.id_user 
            WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            return null;
        }

        return new User(
            $user['id_user'],
            $user['email'],
            $user['password'],
            is_null($user['First_name']) ? '' : $user['First_name'],
            is_null($user['Surname']) ? '' : $user['Surname'],
            is_null($user['phone_number']) ? '' : $user['phone_number'],
            is_null($user['city']) ? '' : $user['city'],
            is_null($user['role']) ? 'User' : $user['role'],
            is_null($user['address']) ? '' : $user['address'],
            is_null($user['id_cart']) ? null : $user['id_cart']
        );
    }

    public function registerUser(
        $email,
        $password,
        $firstName,
        $surName,
        $phone_number,
        $city
    ): ?int
    {
        $stmt = $this->database->connect()->prepare('
             WITH u AS (
                INSERT into public.users (email, password)
                    values (:email, :password)
                    returning id_user
             )
            insert into public.users_details (id_user, "First_name", "Surname", phone_number, city)
            select id_user, :firstname, :surname, :phonenumber, :city
            from u returning id_user
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':firstname', $firstName, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surName, PDO::PARAM_STR);
        $stmt->bindParam(':phonenumber', $phone_number, PDO::PARAM_STR);
        $stmt->bindParam(':city', $city, PDO::PARAM_STR);

        $stmt->execute();

        $userId = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($userId == false) {
            return null;
        }
        return $userId['id_user'];
    }


}


