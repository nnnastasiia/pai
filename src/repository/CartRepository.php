<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/../models/CartItem.php';

class CartRepository extends Repository
{
    public function getCartProductsIds($userId): ?array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.cart_items WHERE id_user = :id
        ');

        $stmt->bindParam(':id', $userId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchALl(PDO::FETCH_ASSOC);

        if ($result == false) {
            return null;
        }

        $cartProducts = array();

        foreach ($result as $product) {
            $cartProducts[] = $product['id_product'];
        }
        return $cartProducts;
    }

    public function getCartProducts($userId): ?array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.cart_items c 
                JOIN public.products p ON p.id_product = c.id_product
            WHERE id_user = :id
        ');

        $stmt->bindParam(':id', $userId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchALl(PDO::FETCH_ASSOC);

        if ($result == false) {
            return null;
        }

        $cartItems = array();

        foreach ($result as $item) {
            $cartItems[] = new CartItem(
                $userId,
                $item['id_product'],
                $item['id_cart_item'],
                new Product(
                    $item['nameOfProduct'],
                    $item['description'],
                    $item['product_image'],
                    $item['price'],
                    $item['id_category'],
                    $item['id_product']
                )
            );
        }
        return $cartItems;
    }

    public function addProductToCart($userId, $productId): bool
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.cart_items 
                ("id_user", "id_product")
            VALUES (?, ?)
        ');
        return $stmt->execute([
            $userId,
            $productId,
        ]);
    }

    public function deleteProductFromCart($cartItemId): bool
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.cart_items WHERE id_cart_item = :itemId
        ');
        $stmt->bindParam(':itemId', $cartItemId, PDO::PARAM_INT);

        return $stmt->execute();

    }
}
