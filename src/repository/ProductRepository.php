<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Product.php';

class ProductRepository extends Repository
{
    public function getAll($number = null)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.products
        ' . ($number ? 'LIMIT :number' : ''));

        if ($number) {
            $stmt->bindParam(':number', $number, PDO::PARAM_INT);
        }

        $stmt->execute();

        $result = $stmt->fetchALl(PDO::FETCH_ASSOC);


        if ($result == false) {
            return null;
        }

        $products = array();

        foreach ($result as $product) {
            $products[] = new Product(
                $product['nameOfProduct'],
                $product['description'],
                $product['product_image'],
                $product['price'],
                $product['id_category'],
                $product['id_product']
            );
        }
        return $products;
    }

    public function getById($id): ?Product
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.products where id_product = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);


        if ($result == false) {
            return null;
        }

        return new Product(
            $result['nameOfProduct'],
            $result['description'],
            $result['product_image'],
            $result['price'],
            $result['id_category'],
            $result['id_product']
        );
    }

    public function addProduct($name, $desc, $catId, $price, $img = null): ?Product
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.products 
                ("nameOfProduct", "description", "product_image", "price", "id_category")
            VALUES (?, ?, ?, ?, ?) returning *
        ');
        $stmt->execute([
            $name,
            $desc,
            $img,
            $price,
            $catId
        ]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);


        if ($result == false) {
            return null;
        }

        return new Product(
            $result['nameOfProduct'],
            $result['description'],
            $result['product_image'],
            $result['price'],
            $result['id_category'],
            $result['id_product']
        );
    }

    public function deleteProduct(int $id): bool
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.products WHERE id_product = :id returning product_image
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result == false) {
            return false;
        }

        unlink('/app/' . $result['product_image']);
        return true;

    }
}
