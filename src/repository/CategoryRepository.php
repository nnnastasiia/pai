<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Category.php';

class CategoryRepository extends Repository
{
    public function getAll($number = null)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.categories
        ' . ($number ? 'LIMIT :number' : ''));

        if ($number) {
            $stmt->bindParam(':number', $number, PDO::PARAM_INT);
        }

        $stmt->execute();

        $result = $stmt->fetchALl(PDO::FETCH_ASSOC);


        if ($result == false) {
            return null;
        }

        $categories = array();

        foreach ($result as $category) {
            $cat = new Category(
                $category['name_of_category'],
                $category['description_of_category'],
                $category['category_image_url'],
                $category['id_category']
            );
            $categories[] = $cat;
        }
        return $categories;
    }

    public function addCategory($name, $desc, $img = null): ?Category
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.categories ("name_of_category", "description_of_category", "category_image_url")
            VALUES (?, ?, ?) returning *
        ');
        $stmt->execute([
            $name,
            $desc,
            $img
        ]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);


        if ($result == false) {
            return null;
        }

        return new Category(
            $result['name_of_category'],
            $result['description_of_category'],
            $result['category_image_url'],
            $result['id_category']
        );
    }

    public function deleteCategory(int $id): bool
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.categories WHERE id_category = :id returning category_image_url
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result == false) {
            return false;
        }

        unlink('/app/' . $result['category_image_url']);
        return true;

    }
}
