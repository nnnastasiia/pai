<?php

class Product
{
    private $idProduct;
    private $nameOfProduct;
    private $descriptionOfProduct;
    private $productImageUrl;
    private $productPrice;
    private $categoryId;


    public function __construct(
        string $nameOfProduct,
        string $descriptionOfProduct,
        string $productImageUrl,
        int    $productPrice,
        int    $categoryId,
        int    $idProduct = null
    )
    {
        $this->nameOfProduct = $nameOfProduct;
        $this->descriptionOfProduct = $descriptionOfProduct;
        $this->productImageUrl = $productImageUrl;
        $this->categoryId = $categoryId;
        $this->productPrice = $productPrice;
        $this->idProduct = $idProduct;
    }

    public function getName()
    {
        return $this->nameOfProduct;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function getProductPrice()
    {
        return $this->productPrice;
    }

    public function setName(string $name)
    {
        $this->nameOfProduct = $name;
    }

    public function getDescription()
    {
        return $this->descriptionOfProduct;
    }

    public function setDescription(string $desc)
    {
        $this->descriptionOfProduct = $desc;
    }

    public function getImg()
    {
        return $this->productImageUrl;
    }

    public function setImg(string $localeUrl)
    {
        $this->productImageUrl = $localeUrl;
    }

    public function getId()
    {
        return $this->idProduct;
    }

    public function setId(int $id)
    {
        $this->idProduct = $id;
    }
}
