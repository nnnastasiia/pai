<?php

class Category
{
    private $idCategory;
    private $nameOfCategory;
    private $descriptionOfCategory;
    private $categoryImageUrl;


    public function __construct(
        string $nameOfCategory,
        string $descriptionOfCategory,
        string $categoryImageUrl,
        int    $idCategory = null
    )
    {
        $this->nameOfCategory = $nameOfCategory;
        $this->descriptionOfCategory = $descriptionOfCategory;
        $this->categoryImageUrl = $categoryImageUrl;
        $this->idCategory = $idCategory;
    }

    public function getName()
    {
        return $this->nameOfCategory;
    }

    public function setName(string $name)
    {
        $this->nameOfCategory = $name;
    }

    public function getDescription()
    {
        return $this->descriptionOfCategory;
    }

    public function setDescription(string $desc)
    {
        $this->descriptionOfCategory = $desc;
    }

    public function getImg()
    {
        return $this->categoryImageUrl;
    }

    public function setImg(string $localeUrl)
    {
        $this->categoryImageUrl = $localeUrl;
    }

    public function getId()
    {
        return $this->idCategory;
    }

    public function setId(int $id)
    {
        $this->idCategory = $id;
    }
}
