<?php

require_once __DIR__ . '/Product.php';

class CartItem
{

    private $idUser;
    private $idProduct;
    private $idCartItem;
    private $product;

    public function __construct(
        int     $idUser,
        int     $idProduct,
        int     $idCartItem,
        Product $product = null
    )
    {
        $this->idUser = $idUser;
        $this->idProduct = $idProduct;
        $this->idCartItem = $idCartItem;
        $this->product = $product;
    }


    public function getIdProduct(): int
    {
        return $this->idProduct;
    }


    public function getIdCartItem(): int
    {
        return $this->idCartItem;
    }

    public function getIdUser(): int
    {
        return $this->idUser;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

}
