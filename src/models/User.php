<?php

class User
{
    private $email;
    private $password;
    private $name;
    private $surname;
    private $idCart;
    private $phoneNumber;
    private $city;
    private $address;
    private $role;
    private $idUser;


    public function __construct(
        int    $idUser,
        string $email,
        string $password,
        string $name,
        string $surname,
        string $phoneNumber,
        string $city,
        string $role,
        string $address = null,
        int    $idCart = null
    )
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->phoneNumber = $phoneNumber;
        $this->city = $city;
        $this->address = $address;
        $this->idCart = $idCart;
        $this->role = $role;
        $this->idUser = $idUser;
    }


    public function getEmail(): string
    {
        return $this->email;
    }

    public function getId(): string
    {
        return $this->idUser;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function getName(): string
    {
        return $this->name;
    }


    public function getSurname(): string
    {
        return $this->surname;
    }


    public function getIdCart(): int
    {
        return $this->idCart;
    }


    public function getCity(): string
    {
        return $this->city;
    }


    public function getAddress(): string
    {
        return $this->address;
    }


    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getRole(): string
    {
        return $this->role;
    }


}
