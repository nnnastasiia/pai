<?php

class AuthService
{
    private $user;

    public function __construct()
    {
        $this->user = unserialize($_SESSION['user']);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function isAuthed(): bool
    {
        return isset($_SESSION['user']);
    }
}
