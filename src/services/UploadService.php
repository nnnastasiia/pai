<?php

class UploadService
{
    private static $target_dir = "/app/uploads/";
    private static $return_dir = "uploads/";

    public function uploadFile(string $inputName): array
    {
        $imageFileType = strtolower(pathinfo(basename($_FILES[$inputName]["name"]), PATHINFO_EXTENSION));
        $newFileName = sha1(gmdate('Y-m-d H:i:s')) . '.' . $imageFileType;
        $target_file = self::$target_dir . $newFileName;
        $return_url = self::$return_dir . $newFileName;

        // Check if image file is an actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES[$inputName]["tmp_name"]);
            if ($check == false) {
                return array('status' => false, 'message' => "File is not an image.");
            }
        }

        // Check file size
        if ($_FILES[$inputName]["size"] > 500000) {
            return array('status' => false, 'message' => "Sorry, your file is too large.");
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            return array('status' => false, 'message' => "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
        }


        if (move_uploaded_file($_FILES[$inputName]["tmp_name"], $target_file)) {
            return array('status' => true, 'message' => $return_url);
        } else {
            return array('status' => false, 'message' => "Sorry, there was an error uploading your file.");
        }
    }
}
