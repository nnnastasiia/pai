<?php

class AdminService
{
    public function isAdmin(): bool
    {
        $user = unserialize($_SESSION['user']);
        return $user->getRole() == 'Admin';
    }
}
