<?php

session_start();
require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('', 'SecurityController');
Routing::post('login', 'SecurityController');
Routing::post('registration', 'SecurityController');
Routing::get('logout', 'SecurityController');

Routing::get('mainpage', 'DefaultController');
Routing::get('katalog', 'DefaultController');
Routing::get('productPage', 'DefaultController');
Routing::get('shoppingCart', 'DefaultController');
Routing::get('payment', 'DefaultController');
Routing::get('userPage', 'DefaultController');

Routing::get('addToCart', 'DefaultController');
Routing::get('deleteCartItem', 'DefaultController');

Routing::get('adminpanel', 'AdminController');
Routing::get('deleteCategory', 'AdminController');
Routing::get('deleteProduct', 'AdminController');


Routing::run($path);
?>
