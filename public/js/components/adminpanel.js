document.addEventListener('DOMContentLoaded', function () {
    var coll = document.getElementsByClassName("button-collapsible");

    for (let i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            this.classList.toggle("active-collapse");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
});


function removeCategory(id) {
    const butn = document.getElementById('category-delete-button-' + id);
    const btnText = butn.innerText;
    butn.innerText = 'Deleting...';
    fetch('/deleteCategory?categoryId=' + id, {method: "GET"}).then((res) => {
        if (res.status === 200) {
            document.getElementById("admin-category-" + id).remove();
        } else {
            alert(res.text());
        }
    }).catch((err) => alert(err)).finally(() => butn.innerText = btnText);
}

function removeProduct(id) {
    const butn = document.getElementById('product-delete-button-' + id);
    const btnText = butn.innerText;
    butn.innerText = 'Deleting...';
    fetch('/deleteProduct?productId=' + id, {method: "GET"}).then((res) => {
        if (res.status === 200) {
            document.getElementById("admin-product-" + id).remove();
        } else {
            alert(res.text());
        }
    }).catch((err) => alert(err)).finally(() => butn.innerText = btnText);
}

