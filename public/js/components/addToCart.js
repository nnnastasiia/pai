function addToCart(id, elementId) {
    const butn = document.getElementById(elementId);
    const btnText = butn.innerText;
    butn.innerText = 'Adding...';
    fetch('/addToCart?productId=' + id, {method: "GET"}).then((res) => {
        if (res.status === 200) {
            butn.innerText = 'Dodano do koszyka';
            butn.disabled = true;
        } else {
            butn.innerText = btnText;
            alert(res.text());
        }
    }).catch((err) => alert(err));
}
