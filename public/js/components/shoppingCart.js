function deleteItemOfCart(id) {
    const elem = document.getElementById('cart-item-' + id);
    const itemPrice = document.getElementById('cart-item-price-' + id);
    const totalPrice = document.getElementById('cart-item-total-price');
    const btn = document.getElementById('cart-item-button-' + id);
    const btnText = btn.innerText;
    btn.innerText = 'Deleting...';
    fetch('/deleteCartItem?itemId=' + id, {method: "GET"}).then((res) => {
        if (res.status === 200) {
            totalPrice.innerText = (parseInt(totalPrice.innerText) - parseInt(itemPrice.innerText)) + '';
            elem.remove();
        } else {
            btn.innerText = btnText;
            alert(res.text());
        }
    }).catch((err) => alert(err));
}
