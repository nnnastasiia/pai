<?php function renderProduct($product, bool $isAuth = false, bool $inCart = false)
{
    ob_start(); ?>
    <div class="card">
        <a href="/productPage?id=<?php echo($product->getId()) ?>">
            <img src="<?php echo($product->getImg()) ?>" alt="">
        </a>
        <div class="rating-result">
            <span class="active"></span>
            <span class="active"></span>
            <span class="active"></span>
            <span></span>
            <span></span>
        </div>
        <div class="caption">
            <a href="/productPage?id=<?php echo($product->getId()) ?>">
                <h3><?php echo $product->getName() ?></h3>
            </a>
            <a href="/productPage?id=<?php echo($product->getId()) ?>">
                <p><?php echo $product->getDescription() ?></p>
            </a>
            <div class="caption-cena-and-like">
                <div>
                    <p id="cena" class="p-text-dark"> <?php echo $product->getProductPrice() ?> zł </p>
                </div>
                <div class="liked-heart">
                    <a class="liked" href=""><img src="/public/images/liked.svg" alt=""></a>
                </div>
            </div>

            <button <?php echo ($inCart || !$isAuth) ? 'disabled' : '' ?>
                    class="button-product"
                    id="<?php echo 'product-' . $product->getId() ?>"
                    onclick="addToCart(<?php echo($product->getId()) ?> , '<?php echo 'product-' . $product->getId() ?>')">
                <?php echo $inCart ? 'Dodano do koszyka' : 'Dodaj do koszyka' ?>
            </button>
        </div>
        <div class="mostliked">
            <p class="mostliked-text">Most liked</p>
        </div>
    </div>

    <?php
    return ob_get_clean();
} ?>
