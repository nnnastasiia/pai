<div class="fast-order">
    <div class="images">
        <img src="/public/images/unsplash_6-12MRO2ru0.png">
    </div>

    <div class="order-form">
        <div class="fast-order-content">
            <h2 id="fast-order-title">Zrób szybkie zamówienie</h2>
            <div class="fast-order-description">
                <p class="p-text-white">Podaj swoje imie i nr telefonu <br> Zadzwonimy do ciebie za 5 minut i umówimy się </p>
            </div>
        </div>

        <div class="input-form">
            <div class="input-imie">
                <input type="text" size="20" placeholder="Imię">
            </div>
            <div class="input-phone-number">
                <input type="text" size="9" placeholder="Nr. telefonu : 48 XXX XXX XXX">
            </div>
        </div>

        <button class="button-callme">Zadzwoń do mnie</button>
    </div>
</div>