<header>
    <div class="wrapper">
        <div class="container navbar">
            <a href="/mainpage">
                <div class="logo">Kwiaty</div>
            </a>
            <!-- /.logo -->
            <nav>
                <a href="mainpage#feedbacks">O NAS</a>
                <a href="katalog">SKLEP</a>
                <!--<a href="">KONTAKT</a>-->
                <?php if (isset($_SESSION['user'])) {
                    $user = unserialize($_SESSION['user']); ?>
                    <a href="shoppingCart">
                        <img class="icon" src="/public/images/clarity_shopping-cart-line.svg" alt="">
                    </a>
                    <a href="">
                        <img class="icon" src="/public/images/akar-icons_heart.svg" alt="">
                    </a>
                    <a href="userPage" class="header__button">
                        <img class="icon" src="/public/images/user_icon.svg" alt="">
                        <?php
                        echo $user->getName();
                        ?>
                    </a>

                    <?php
                    if ($user->getRole() == 'Admin') { ?>
                        <a href="adminpanel" class="header__button">
                            Admin panel
                        </a>
                    <?php } ?>

                    <a href="logout" class="header__button">
                        Logout
                    </a>
                <?php } else { ?>
                    <a href="login">
                        Login
                    </a>
                <?php } ?>
            </nav>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.wrapper -->

    <script src="public/js/components/header.js"></script>
    <script src="public/js/components/addToCart.js"></script>
</header>
