<div class="instagram">
    <section class="instagram-posts">
        <div id="inst-post-1">
            <a class="insta-link" href="">
                <img src="/public/images/instagram/post-instagram-1.png">
            </a>
        </div>
        <div id="inst-post-2">
            <a class="insta-link" href="">
                <img src="/public/images/instagram/post-instagram.png">
            </a>
        </div>
        <div id="inst-post-3">
            <h1 id="our-instagram-title" class="title-dark">Nasz Instagram</h1>
            <div class="button-main-dark">
                <a class="dark-button-text" href="">Subscribe</a>
            </div>
        </div>
        <div id="inst-post-4">
            <a class="insta-link" href="">
                <img src="/public/images/instagram/post-instagram-2.png">
            </a>
        </div>

        <div id="inst-post-5">
            <a class="insta-link" href="">
                <img src="/public/images/instagram/post-instagram-3.png">
            </a>
        </div>
    </section>
</div>