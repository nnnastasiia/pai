<div class="feedbacks" id="feedbacks">
    <div class="feedback-title">
        <h1 id="feedbacks-title" class="title-dark"> Nasi klienci o nas</h1>
    </div>
    <section class="feedbacks-cards">
        <div class="feedback-1">
            <img class="quotes-img" src="/public/images/carbon_quotes-card.svg">
            <div class="feedback-card-text">
                <p class="p-text-white">Od wielu lat się zastanawiam czy Justynce i jej pracowitym florystkom skończą się kiedyś pomysły... ? Na szczęście ich kreatywność wydaje się nie mieć granic i każda kompozycja zaskakuje czymś nowym, świeżym i niepowtarzalnym. </p>
            </div>
            <div class="feedback-card-author">
                <p class="p-text-white">-Magda P.</p>
            </div>
        </div>

        <div class="feedback-2">
            <img class="quotes-img" src="/public/images/carbon_quotes-card.svg">
            <div class="feedback-card-text">
                <p class="p-text-white">Cudowne miejsce i cudowne dziewczyny widać, że to co robią to z pasją. </p>
            </div>
            <div class="feedback-card-author">
                <p class="p-text-white">-Łukasz Z.</p>
            </div>
        </div>

        <div class="feedback-3">
            <img class="quotes-img" src="/public/images/carbon_quotes-card.svg">
            <div class="feedback-card-text">
                <p class="p-text-white"> Piękna praca , która bardzo spodobała się naszym gościom weselnym. Dziękujemy za upiększenie najważniejszego dnia w naszym życiu! </p>
            </div>
            <div class="feedback-card-author">
                <p class="p-text-white">-Bartek A.</p>
            </div>
        </div>

        <div class="feedback-4">
            <img class="quotes-img" src="/public/images/carbon_quotes-card.svg">
            <div class="feedback-card-text">
                <p class="p-text-white">Wspaniałe bukiety, nietuzinkowe dekoracje, niesamowite wyczucie estetyczne i radość tworzenia Justyny i jej ekipy - czyli wszystko czego można chcieć w dniu ślubu czy innej imprezie. Polecam serdecznie! </p>
            </div>
            <div class="feedback-card-author">
                <p class="p-text-white">-Elena T.</p>
            </div>
        </div>

    </section>
</div>
