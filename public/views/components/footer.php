<footer>
    <div class="footer">
        <div class="footer-logo">
            <h1 class="title-footer">Kwiaty</h1>
            <hr>
        </div>
        <div class="footer-navig">
            <a class="footer-nav" href="">O Nas</a>
            <a class="footer-nav" href="">Sklep</a>
            <a class="footer-nav" href="">Kontakt</a>
            <a class="footer-nav" href="">Polityka Prywatności</a>
            <a class="footer-nav" href="">Zasady i Warunki</a>
            <a class="footer-nav" href="">polityka wysyłki </a>
        </div>
        <div class="footer-social-icons">
            <a class="footer-soc-icons" href=""> <img src="/public/images/Facebook_white.svg"> </a>
            <a class="footer-soc-icons" href=""> <img src="/public/images/Instagram_white.svg"> </a>
            <a class="footer-soc-icons" href=""> <img src="/public/images/Twitter_white.svg"> </a>
        </div>
        <div class="footer-copyright">
            <p class="copyright">
                Copyright 2022 ©  KWIATY
            </p>
        </div>

    </div>
</footer>
