<link rel="stylesheet" href="/public/css/card.css">



<div class="card card-1">
    <img src="./ph.png" alt="" class="card__img">
    <!-- /.card__img -->
    <div class="card__main">
        <div class="card__text">
            <div class="card__title">Bukiet Adamant</div>
            <!-- /.card__title -->
            <span class="card__product-size"></span>
            <!-- /.card__product-size -->
            <div class="card__event-message">
                <img src="./fav.svg" alt="" class="card__icon">
                Dodano do ulibionych
            </div>
            <!-- /.card__event-message -->
        </div>
        <!-- /.card__text -->
        <div class="card__buttons">
            <a href="#" class="card_continue">Kontynuuj zakupy</a>
            <a href="#" class="card_return">Przejdz do uliubionych</a>
        </div>
        <!-- /.card__buttons -->
    </div>
    <!-- /.card__main -->
    <div class="card__cancel">
        <div class="card__cancel-top"><img src="./cross.svg" alt=""></div>
        <!-- /.card__cancel-top -->
        <div class="cancel-bottom"><span>Anuluj <img src="./cross.svg" alt=""></span></div>
        <!-- /.cancel-bottom -->
    </div>
    <!-- /.card__cancel -->
</div>
<!-- /.card card-1 -->
<br>
<div class="card card-2">
    <img src="./ph.png" alt="" class="card__img">
    <!-- /.card__img -->
    <div class="card__main">
        <div class="card__text">
            <div class="card__title">Bukiet Adamant</div>
            <!-- /.card__title -->
            <span class="card__product-size">-  sredni</span>
            <!-- /.card__product-size -->
            <div class="card__event-message">
                <img src="./cart.svg" alt="" class="card__icon">
                Dodano do koszyku
            </div>
            <!-- /.card__event-message -->
        </div>
        <!-- /.card__text -->
        <div class="card__buttons">
            <a href="#" class="card_return">Przejdz do uliubionych</a>
            <a href="#" class="card_continue">Kontynuuj zakupy</a>
        </div>
        <!-- /.card__buttons -->
    </div>
    <!-- /.card__main -->
    <div class="card__cancel">
        <div class="card__cancel-top"><img src="./cross.svg" alt=""></div>
        <!-- /.card__cancel-top -->
        <div class="cancel-bottom"><span>Anuluj <img src="./cross.svg" alt=""></span></div>
        <!-- /.cancel-bottom -->
    </div>
    <!-- /.card__cancel -->
</div>
<!-- /.card card-2 -->