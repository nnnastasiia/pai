<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/products.css">
    <link rel="stylesheet" type="text/css" href="/public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/productPage.css">
    <link rel="stylesheet" type="text/css" href="/public/css/shoppingCart.css">

    <script src="public/js/components/shoppingCart.js"></script>

    <title>KOSZYK</title>
</head>

<?php include __DIR__ . '/../components/header.php'; ?>

<body class="shopping-cart-body">
<div class="cart-container">
    <div class="cart-title">
        <h1 class="cart-title-dark"> Koszyk </h1>
    </div>

    <div class="cart-content">
        <?php
        $totalPrice = 0;
        if (isset($cartItems)) {
            foreach ($cartItems as $item) {
                $product = $item->getProduct(); ?>
                <div class="cart-product" id="<?php echo 'cart-item-' . $item->getIdCartItem() ?>">
                    <div class="cart-product-img">
                        <img src="<?php echo($product->getImg()) ?>" alt="">
                    </div>

                    <div class="cart-product-name">
                        <h3><?php echo $product->getName() ?></h3>
                    </div>

                    <div class="cart-product-cena">
                        <p class="cart-cena-text"><span
                                    id="<?php echo 'cart-item-price-' . $item->getIdCartItem() ?>"><?php echo $product->getProductPrice() ?></span>
                            PLN </p>
                    </div>


                    <div class="cart-product-delete-btn">
                        <button id="<?php echo 'cart-item-button-' . $item->getIdCartItem() ?>"
                                onclick="deleteItemOfCart(<?php echo $item->getIdCartItem() ?>)">Delete
                        </button>
                    </div>

                </div>
                <?php
                $totalPrice += $product->getProductPrice();
            }
        }
        ?>

        <div class="continue+total">
            <div class="cart-button-continue">
                <a class="button-continue" href="">
                    <p class="cart-button-text"> Kontynuuj zakupy </p>
                </a>
            </div>

            <div class="total-value">
                <p class="total">Razem:
                    <span id="cart-item-total-price"><?php echo $totalPrice ?></span>
                    PLN</p>
            </div>
        </div>

        <div class="cart-accept-button">
            <a class="cart-accept" href="">
                <p class="cart-accept-button-text">KUPUJĘ</p>
            </a>
        </div>

    </div>
</div>


</body>


<!-------------------FOOTER--------------->
<?php include __DIR__ . '/../components/footer.php'; ?>


</html>
