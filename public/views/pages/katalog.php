<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/products.css">
    <link rel="stylesheet" type="text/css" href="/public/css/katalog-style.css">

    <title>KATALOG</title>
</head>

<?php include __DIR__ . '/../components/header.php'; ?>

<body class="katalog-body">

<!-------------FILTRY--------------->

<div class="filters-block">
    <p class="filters-title">Filtry</p>
    <div class="filtry">

        <div class="katalog-cena">
            <a href class="filter-name-link">
                <div class="name-of-filter">
                    <p class="filter-name"> Cena</p>
                </div>
                <div class="filter-chevron">
                    <img src="/public/images/bytesize_chevron-bottom-open.svg" alt="">
                </div>
            </a>
        </div>

        <div class="katalog-okazje">
            <a href class="filter-name-link">
                <div class="name-of-filter">
                    <p class="filter-name"> Okazje</p>
                </div>
                <div class="filter-chevron">
                    <img src="/public/images/bytesize_chevron-bottom-close.svg" alt="">
                </div>
            </a>
        </div>

        <div class="katalog-kategorie">
            <a href class="filter-name-link">
                <div class="name-of-filter">
                    <p class="filter-name"> Kategorie</p>
                </div>
                <div class="filter-chevron">
                    <img src="/public/images/bytesize_chevron-bottom-close.svg" alt="">
                </div>
            </a>
            <a href class="wybierz-kategorie-cont"></a>
            <div class="wyb-kateg-selection-cont">
                <!--        <ul class="kateg-select-list">
                            <li>...</li>
                            <li>...</li>
                            <li class="selected">...</li>
                            <li>...</li>
                        </ul>-->
            </div>
        </div>

        <div class="katalog-kolor">
            <a href class="filter-name-link">
                <div class="name-of-filter">
                    <p class="filter-name"> Według koloru</p>
                </div>
                <div class="filter-chevron">
                    <img src="/public/images/bytesize_chevron-bottom-close.svg" alt="">
                </div>
            </a>
        </div>
        <div class="katalog-najpierw">
            <a href class="filter-name-link">
                <div class="name-of-filter">
                    <p class="filter-name-sort"> Naipierw popularne </p>
                </div>
                <div class="filter-chevron">
                    <img src="/public/images/bytesize_chevron-bottom-close.svg" alt="">
                </div>
            </a>
        </div>
        <div class="display-way">
            <a class="display-grid" href="">
                <img src="/public/images/fe_app-menu.svg" alt="">
            </a>
            <a class="display-list" href="">
                <img src="/public/images/fe_list-bullet.svg" alt="">
            </a>
        </div>
    </div>
</div>


<!---------------PRODUKTY-SKLEP---------------->

<div class="products-katalog">
    <?php include __DIR__ . '/../components/product.php';
    if (isset($products)) {
        ?>
        <section class="products">
            <?php
            foreach ($products as $product) {
                echo renderProduct($product, isset($_SESSION['user']), in_array($product->getId(), $cartProductsIds));
            } ?>

        </section>
    <?php } ?>
</div>


<!-------------PAGINATION------------->
<div class="pagination">
    <a href="#">1</a>
    <a class="active" href="#">2</a>
    <a href="#">3</a>
    <a href="#">4</a>
</div>


<!------------NEWSLETTER----------->
<div class="newsletter-dark">
    <div class="newsletter-title">
        <h1 id="newsletter-title" class="title"> Zapisz się do newslettera </h1>
    </div>
    <div class="newsletter-description">
        <p class="p-text-white"> otrzymuj -30% na pierwsze zamówienie i wcześniejszy dostęp do ofert specjalnych</p>
    </div>
    <div class="input-form">
        <div class="input-email">
            <label>
                <input class="input-email-form-dark" type="email" size="20" placeholder="E-mail: mail@mail.pl">
            </label>
        </div>
    </div>

    <div class="button-wyslij-white">
        <a class="white-button-text" href="">Wyślij</a>
    </div>
</div>


<!---------------NASZ INSTAGRAM--------------->

<?php include __DIR__ . '/../components/instagram-block.php'; ?>


</body>


<!-------------------FOOTER--------------->
<?php include __DIR__ . '/../components/footer.php'; ?>


</html>
