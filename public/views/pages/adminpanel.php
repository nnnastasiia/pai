<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/adminpanel.css">
    <link rel="stylesheet" type="text/css" href="public/css/products.css">
    <!-- Connect libs -->
    <script src="public/js/components/adminpanel.js"></script>

    <title>Admin Panel</title>
</head>

<body>
<?php include __DIR__ . '/../components/header.php'; ?>

<div class="messages">
    <?php
    if (isset($messages)) {
        foreach ($messages as $message) {
            echo $message;
        }
    }
    ?>
</div>

<div class="creating-wrapper">
    <form action="adminpanel" method="post" enctype="multipart/form-data">
        <h2>Create Category</h2>
        <label>
            Category Name:
            <input name="category_name" class="form-input" type="text" size="255" placeholder="Category Name">
        </label>
        <label>
            Category Description:
            <input name="category_desc" class="form-input" type="text" placeholder="Category Description">
        </label>
        <label>
            Select image to upload:
            <input class="file-input" type="file" name="categoryFile" id="categoryFile">
        </label>
        <hr class="form__hr">
        <input class="button__submit" type="submit" value="Create Category" name="submit">
    </form>


    <form action="adminpanel" method="post" enctype="multipart/form-data">

        <h2>Create Product</h2>
        <label>
            Product Name:
            <input required name="product_name" class="form-input" type="text" size="255" placeholder="Product Name">
        </label>
        <label>
            Product Description:
            <input required name="product_desc" class="form-input" type="text" placeholder="Product Description">
        </label>
        <label>
            Product Price:
            <input required name="product_price" class="form-input" type="number" min="0" placeholder="Product Price">
        </label>

        <label>
            Category:
            <select class="form-input" name="product_category_id">
                <option selected value="null" disabled>Choose a category</option>
                <?php
                if (isset($categories)) {
                    foreach ($categories as $category) { ?>
                        <option value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
                    <?php }
                }
                ?>
            </select>
        </label>
        <label>
            Select image to upload:
            <input class="file-input" type="file" name="productFile" id="productFile">
        </label>
        <hr class="form__hr">
        <input class="button__submit" type="submit" value="Create Product" name="submit">
    </form>
</div>

<div class="admin__categories">
    <?php
    if (isset($categories)) {
        foreach ($categories as $category) { ?>
            <div class="admin__category" id="<?php echo 'admin-category-' . $category->getId() ?>">
                <div class="admin__category__details">
                    <div class="admin__category__details__data">
                        <img height="150" width="auto" src="<?php echo $category->getImg() ?>"
                             alt="<?php echo $category->getName() ?>">
                        <div>
                            <h3><?php echo $category->getName() ?> </h3>
                            <p><?php echo $category->getDescription() ?></p>
                        </div>
                    </div>
                    <div class="admin__category__details__button">
                        <button onclick="removeCategory(<?php echo $category->getId() ?>)"
                                id="<?php echo 'category-delete-button-' . $category->getId() ?>">
                            DELETE
                        </button>
                    </div>
                </div>
                <button type="button" class="button-collapsible">Products</button>
                <div class="admin__products-wrapper">
                    <div class="products">
                        <?php
                        if (isset($products)) {
                            foreach ($products as $product) {
                                if ($category->getId() == $product->getCategoryId()) { ?>
                                    <div class="card" id="<?php echo 'admin-product-' . $product->getId() ?>">
                                        <img src="<?php echo $product->getImg() ?>" alt="">
                                        <div class="rating-result">
                                            <span class="active"></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                        <div class="caption">
                                            <div>
                                                <h3><?php echo $product->getName() ?></h3>
                                            </div>
                                            <div>
                                                <p><?php echo $product->getDescription() ?></p>
                                            </div>
                                            <div class="caption-cena-and-like">
                                                <div>
                                                    <p id="cena"
                                                       class="p-text-dark"> <?php echo $product->getProductPrice() ?> zł </p>
                                                </div>
                                            </div>

                                            <div class="admin__product__details__button">
                                                <button onclick="removeProduct(<?php echo $product->getId() ?>)"
                                                        id="<?php echo 'product-delete-button-' . $product->getId() ?>">
                                                    DELETE
                                                </button>
                                            </div>
                                        </div>
                                        <div class="mostliked">
                                            <p class="mostliked-text">Most liked</p>
                                        </div>
                                    </div>

                                <?php }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php }
    }
    ?>
</div>

<?php include __DIR__ . '/../components/footer.php'; ?>
</body>
</html>
