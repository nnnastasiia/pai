<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/products.css">
    <link rel="stylesheet" type="text/css" href="public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="public/css/productPage.css">
    <link rel="stylesheet" type="text/css" href="public/css/shoppingCart.css">
    <link rel="stylesheet" type="text/css" href="public/css/payment.css">
    <link rel="stylesheet" type="text/css" href="public/css/login.css">


    <title>LOGIN</title>
</head>



<body class="login-body">

    <div class="login-container">
        <div class="login-image-container">
            <div class="image-login-cover">
                <img src="/public/images/Rectangle%20602.png" alt="">
            </div>
        </div>


        <div class="login-form-container">
            <div class="login-form-title">
                <h1 class="login-title">LOGIN</h1>
            </div>
            <div class="login-form">
                <form class="login" action="login" method="POST">
                    <div class="messages">
                        <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                        ?>
                    </div>

                    <label>
                        <input name="email" class="login-input" type="email" size="30" placeholder="E-mail: email@mail.pl">
                    </label>
                    <label>
                        <input name="password" class="login-input" type="password" size="30" placeholder="Password">
                    </label>

                    <p class="login-text"> Lub zaloguj się za pomocą </p>
                <div class="login-icons">
                    <a href=""><img src="/public/images/akar-icons_facebook-fill.svg" alt=""></a>
                    <a href=""><img src="/public/images/flat-color-icons_google.svg" alt=""></a>
                </div>

                    <button class="login-button" type="submit">Zaloguj się</button>
                </form>
            </div>



            <div class="register-container">
                <p class="register-text">Nie masz konta?</p>
                <a class="register-link" href="/registration"> Zarejestruj się </a>
            </div>
        </div>

    </div>






</body>
