<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/products.css">
    <link rel="stylesheet" type="text/css" href="/public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/productPage.css">
    <link rel="stylesheet" type="text/css" href="/public/css/shoppingCart.css">
    <link rel="stylesheet" type="text/css" href="/public/css/payment.css">
    <link rel="stylesheet" type="text/css" href="/public/css/login.css">
    <link rel="stylesheet" type="text/css" href="/public/css/registration.css">


    <title>REGISTRATION</title>
</head>


<body class="registration-body">

<div class="registration-form-title">
    <h1 class="registration-title">registration</h1>
</div>

<div class="registration-form">
    <form class="registration" action="registration" method="POST">

        <div class="messages">
            <?php
            if(isset($messages)){
                foreach($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>

        <label>
            <input name="firstname" class="registration-input" type="text" size="30" placeholder="Imię">
        </label>
        <label>
            <input name="surname" class="registration-input" type="text" size="30" placeholder="Nazwisko">
        </label>
        <label>
            <input name="email" class="registration-input" type="email" size="30" placeholder="E-mail: email@mail.pl">
        </label>
        <label>
            <input name="password" class="registration-input" type="password" size="30" placeholder="Password">
        </label>
        <label>
            <input name="phone_number" class="registration-input" type="text" size="13" placeholder="Nr telefonu">
        </label>
        <label>
            <input name="city" class="registration-input" type="text" size="30" placeholder="Miasto">
        </label>


        <button class="registration-button" type="submit">Zarejestruj się</button>

    </form>

</div>



</body>

</html>
