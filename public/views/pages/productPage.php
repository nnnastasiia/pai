<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/products.css">
    <link rel="stylesheet" type="text/css" href="public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="public/css/productPage.css">

    <title>Bukiet Adamant</title>
</head>

<?php include __DIR__ . '/../components/header.php'; ?>


<body class="productPage-body">

<!------------PRODUCT-CONTENT------------->

<div class="productPage-container">
    <div class="product-content">
        <?php if (isset($product)) { ?>

        <div class="product-title">
            <h1 class="productPage-title"> <?php echo $product->getName() ?> </h1>
        </div>

        <div class="product-cena">
            <p class="name-of-desc"> Cena: <?php echo $product->getProductPrice() ?> PLN </p>
            <p class="cena-description"> * łącznie z dostawą </p>
        </div>

        <div class="size-of-product">
            <div class="select-size">
                <p class="name-of-desc"> Wielkość bukietu </p>
                <div class="button-size">
                    <button class="select-size-button">mały
                        <hr>
                        <p class="size-description"> + 30,00 PLN </p>
                    </button>
                    <button class="select-size-button">średni
                        <hr>
                        <p class="size-description"> + 45,00 PLN </p>
                    </button>
                    <button class="select-size-button"> duży
                        <hr>
                        <p class="size-description"> + 55,00 PLN </p>
                    </button>
                </div>
            </div>
        </div>

        <div class="product-description">
            <p class="product-desc"> <?php echo $product->getDescription() ?> </p>
            <p class="product-desc"> Mniejszy - ok. 10 kwiatów i przybranie </p>
            <p class="product-desc"> Średni - ok. 15 kwiatów i przybranie </p>
            <p class="product-desc"> Większy - ok. 20 kwiatów i przybranie </p>
        </div>

        <button class="button-add-to-cart">Dodaj do koszyka</button>


    </div>

    <div class="product-image+links">
        <div class="product-image">
            <img src="<?php echo $product->getImg() ?>" alt="">
        </div>

        <div class="privacy-links">
            <a class="privacy-link" href="">Polityka prywatności</a>
            <a class="privacy-link" href="">Zasady i Warunki</a>
            <a class="privacy-link" href="">polityka wysyłki </a>
        </div>

        <?php } else { ?>
            <div class="messages">
                <?php
                if (isset($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
        <?php } ?>
    </div>
</div>
<hr>


<!--FAST ORDER-->
<?php include __DIR__ . '/../components/fast-order-block.php'; ?>


<!--------------POLECANE-BUKIETY------------>
<div class="recommended-products">
    <div class="recommended-title">
        <h1 id="recommender-products-title" class="title-dark">Polecane bukiety</h1>
    </div>
    <section class="products">
        <div class="card" id="prod-card-1">
            <img src="/public/images/products/Rectangle%20110-1.png" alt="">
            <div class="rating-result">
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
            <div class="caption">
                <div>
                    <h3>Adamant</h3>
                </div>
                <div class="caption-cena-and-like">
                    <div>
                        <p id="cena" class="p-text-dark"> 300 zł </p>
                    </div>
                    <div class="liked-heart">
                        <a class="liked" href=""><img src="/public/images/liked.svg" alt=""></a>
                    </div>
                </div>

                <button class="button-product">Dodaj do koszyka</button>
            </div>
            <div class="mostliked">
                <p class="mostliked-text">Most liked</p>
            </div>
        </div>
        <div class="card" id="prod-card-2">
            <img src="/public/images/products/Rectangle%20110-1.png" alt="">
            <div class="rating-result">
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
            <div class="caption">
                <div>
                    <h3>Adamant</h3>
                </div>
                <div class="caption-cena-and-like">
                    <div>
                        <p id="cena" class="p-text-dark"> 300 zł </p>
                    </div>
                    <div class="liked-heart">
                        <a class="liked" href=""><img src="/public/images/liked.svg" alt=""></a>
                    </div>
                </div>

                <button class="button-product">Dodaj do koszyka</button>
            </div>
            <div class="mostliked">
                <p class="mostliked-text">Most liked</p>
            </div>
        </div>
        <div class="card" id="prod-card-3">
            <img src="/public/images/products/Rectangle%20110-1.png" alt="">
            <div class="rating-result">
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
            <div class="caption">
                <div>
                    <h3>Adamant</h3>
                </div>
                <div class="caption-cena-and-like">
                    <div>
                        <p id="cena" class="p-text-dark"> 300 zł </p>
                    </div>
                    <div class="liked-heart">
                        <a class="liked" href=""><img src="/public/images/liked.svg" alt=""></a>
                    </div>
                </div>

                <button class="button-product">Dodaj do koszyka</button>
            </div>
            <div class="mostliked">
                <p class="mostliked-text">Most liked</p>
            </div>
        </div>
        <div class="card" id="prod-card-4">
            <img src="/public/images/products/Rectangle%20110-1.png" alt="">
            <div class="rating-result">
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
            <div class="caption">
                <div>
                    <h3>Adamant</h3>
                </div>
                <div class="caption-cena-and-like">
                    <div>
                        <p id="cena" class="p-text-dark"> 300 zł </p>
                    </div>
                    <div class="liked-heart">
                        <a class="liked" href=""><img src="/public/images/liked.svg" alt=""></a>
                    </div>
                </div>

                <button class="button-product">Dodaj do koszyka</button>
            </div>
            <div class="mostliked">
                <p class="mostliked-text">Most liked</p>
            </div>
        </div>

    </section>

</div>


<!-----------NASI KLIENCI O NAS ----------->
<?php include __DIR__ . '/../components/reviews.php'; ?>


</body>


<!-------------------FOOTER--------------->
<?php include __DIR__ . '/../components/footer.php'; ?>


</html>
