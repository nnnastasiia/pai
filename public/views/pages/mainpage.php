<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/products.css">

    <title>KWIATY</title>
</head>


<body class="mainpage-body">

<?php include __DIR__ . '/../components/header.php'; ?>

<!--MAIN_PAGE-->
<div class="container-main-page">
    <div class="images">
        <img src="/public/images/unsplash_7pGehyH7o64.png" alt="">
    </div>
    <div class="title">
        <h1>Najlepszy wybór Bukiet “Amarant”</h1>
        <div class="description-mainpage">
            <p class="description-left">Piony, tulipany</p>
            <p class="description-right">Dla dziewczyny, na urodziny,dla matki</p>
        </div>

        <div class="button-main">
            <a class="white-link-text" href="">DOWIEDŻ SIĘ WIĘCEJ</a>
        </div>
    </div>
</div>


<!--KATEGORIE-->
<?php
if (isset($categories)) { ?>
    <div class="category-wrapper">
        <section class="categories">
            <?php foreach ($categories

                           as $category) { ?>
                <div class="category">
                    <div style="
                            background: url(<?php echo $category->getImg() ?>) no-repeat;
                            background-size: cover;
                            filter: blur(3px);
                            -webkit-filter: blur(3px);"
                    ></div>
                    <div class="darken"></div>
                    <div class="content">
                        <p class="catg-title"> <?php echo $category->getName() ?></p>
                        <p class="catg-description"> <?php echo $category->getDescription() ?> </p>
                        <div class="arrow-link">
                            <a class="arrow-text" href="">Więcej
                                <div class="arrow">
                                    <img src="/public/images/Arrow%203.svg" alt="">
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
            <?php } ?>

        </section>
    </div>
<?php } ?>


<!--Najbardziej popularne-->

<?php
if (isset($products)) { ?>
    <div class="most-popular">
        <div class="title">
            <h1 id="most-popular-title" class="title-dark">Najbardziej popularne</h1>
        </div>
        <section class="products">
            <?php
            include __DIR__ . '/../components/product.php';
            foreach ($products as $product) {
                echo renderProduct($product, isset($_SESSION['user']), in_array($product->getId(), $cartProductsIds));
            } ?>

        </section>

    </div>
<?php } ?>


<!--FAST ORDER-->

<?php include __DIR__ . '/../components/fast-order-block.php'; ?>


<!-------NASZ INSTAGRAM------>
<?php include __DIR__ . '/../components/instagram-block.php'; ?>


<!-----------NASI KLIENCI O NAS ----------->
<?php include __DIR__ . '/../components/reviews.php'; ?>


<!------------NEWSLETTER----------->
<div class="newsletter-light">
    <div class="newsletter-title">
        <h1 id="newsletter-title" class="title-dark"> Zapisz się do newslettera </h1>
    </div>
    <div class="newsletter-description">
        <p class="p-text-dark"> otrzymuj -30% na pierwsze zamówienie i wcześniejszy dostęp do ofert specjalnych</p>
    </div>
    <div class="input-form">
        <div class="input-email">
            <label>
                <input class="input-email-form" type="email" size="20" placeholder="E-mail: mail@mail.pl">
            </label>
        </div>
    </div>

    <button class="button-wyslij-dark">Wyślij</button>
</div>


</body>

<!-------------------FOOTER--------------->

<?php include __DIR__ . '/../components/footer.php'; ?>
</html>
