<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/products.css">
    <link rel="stylesheet" type="text/css" href="/public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/productPage.css">
    <link rel="stylesheet" type="text/css" href="/public/css/shoppingCart.css">
    <link rel="stylesheet" type="text/css" href="/public/css/payment.css">


    <title>DOSTAWA I OPŁATA</title>
</head>


<?php include __DIR__ . '/../components/header.php'; ?>

<body class="payment-body">

<div class="payment-container">
    <div class="shipping-and-payment-container">
        <div class="payment-title">
            <h4 class="paym-title">Dostawa i Opłata </h4>
        </div>

        <div class="user-shipping-information">
            <div class="paym-input-imie">
                <label>
                    <input class="payment-form" type="text" size="20" placeholder="Imię">
                </label>
            </div>
            <div class="paym-input-nazwisko">
                <label>
                    <input class="payment-form" type="text" size="20" placeholder="Nazwisko">
                </label>
            </div>
            <div class="paym-input-ulica">
                <label>
                    <input class="payment-form" type="text" size="25" placeholder="Ulica">
                </label>
            </div>
            <div class="paym-input-nrdomu">
                <label>
                    <input class="payment-form" type="text" size="5" placeholder="Nr domu">
                </label>
            </div>
            <div class="paym-input-miasto">
                <label>
                    <input class="payment-form" type="text" size="20" placeholder="Miejscowość">
                </label>
            </div>
            <div class="paym-input-postcode">
                <label>
                    <input class="payment-form" type="number" size="5" placeholder="Kod pocztowy">
                </label>
            </div>
            <div class="paym-input-phonenumb">
                <label>
                    <input class="payment-form" type="number" size="20" placeholder="Nr telefonu">
                </label>
            </div>
            <div class="paym-input-wishes">
                <label>
                    <input class="payment-form" type="text" size="100" placeholder="Życzenia / Treść bilecika">
                </label>
            </div>
            <div class="paym-input-notes">
                <label>
                    <input class="payment-form" type="text" size="100" placeholder="Uwagi dla kuriera">
                </label>
            </div>

        </div>

        <div class="payment-method-container">
            <div class="payment-method-title">
                <h4 class="paym-meth-title"> Sposób opłaty</h4>
            </div>
            <div class="method-buttons">
                <button class="select-method-button">Karta</button>
                <button class="select-method-button">GooglePay</button>
                <button class="select-method-button">ApplePay</button>
                <button class="select-method-button">BLIK</button>
            </div>

            <div class="inputform-card-info">
                <div class="paym-input-nrkarty">
                    <label>
                        <input class="payment-form" type="text" size="16" placeholder="Nr karty">
                    </label>
                </div>

                <div class="paym-input-due">
                    <label>
                        <input class="payment-form" type="date" size="5" placeholder="Ważność">
                    </label>
                </div>

                <div class="paym-input-cvc">
                    <label>
                        <input class="payment-form" type="text" size="3" placeholder="CVC-1">
                    </label>
                </div>
            </div>
        </div>

        <button class="button-accept-order">Zloż zamówienie</button>

    </div>


    <div class="order-information-container">
        <div class="order-info-title">
            <h4 class="order-info-title-text">Zamówienie</h4>
        </div>

        <div class="cart-container">
            <div class="cart-content">
                <div class="cart-product">
                    <div class="cart-product-img">
                        <img src="/public/images/Rectangle%20110-paym.png" alt="">
                    </div>

                    <div class="cart-product-name">
                        <h3>Adamant</h3>
                    </div>

                    <div class="cart-prduct-cena">
                        <p class="cart-cena-text"> 140 PLN </p>
                    </div>

                    <div class="cart-product-count">

                    </div>

                    <div class="cart-product-value">
                        <p class="cart-cena-text"> 140 PLN </p>
                    </div>

                </div>
            </div>
        </div>


        <div class="total-value">
            <p class="total">Razem: 420 PLN</p>
        </div>

</div>


</div>


</body>



<!-------------------FOOTER--------------->
<?php include __DIR__ . '/../components/footer.php';?>


</html>
