<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/products.css">
    <link rel="stylesheet" type="text/css" href="public/css/katalog-style.css">
    <link rel="stylesheet" type="text/css" href="public/css/productPage.css">
    <link rel="stylesheet" type="text/css" href="public/css/shoppingCart.css">
    <link rel="stylesheet" type="text/css" href="public/css/payment.css">
    <link rel="stylesheet" type="text/css" href="public/css/userPage.css">


    <title>Użytkownik</title>
</head>


<?php $user = unserialize($_SESSION['user']); ?>

<body class="userpage-body">
<?php include __DIR__ . '/../components/header.php'; ?>


<div class="userpage-container">
    <div class="userpage-title">
        <h4 class="up-title">Informacja użytkownika</h4>
    </div>
    <div class="user-information-container">
        <div class="user-info-desc">
            <p class="user-info-description-title">Imię</p>
            <div class="user-info-value-cont">
                <!--                <label>
                                    <input type="text"  name="name" value="user_imie" readonly>
                                    </label>-->
                <p class="user-info-text"> <?php echo $user->getName(); ?> </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>

        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">Nazwisko</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text"> <?php echo $user->getSurname(); ?> </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>

            </div>
        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">E-mail</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text">
                    <?php
                    echo $user->getEmail();
                    ?>
                </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>
        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">Numer telefonu</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text">
                    <?php
                    echo $user->getPhoneNumber();
                    ?>
                </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>
        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">Miasto</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text">
                    <?php
                    echo $user->getCity();
                    ?>
                </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>
        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">Adres</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text">
                    <?php
                    echo $user->getAddress();
                    ?>
                </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>
        </div>
        <div class="user-info-desc">
            <p class="user-info-description-title">Dane karty platniczej</p>
            <div class="user-info-value-cont">
                <!--<label>
                    <input type="text"  name="name" value="user_imie" readonly>
                    </label>-->
                <p class="user-info-text">
                    not implemented
                </p>
                <a class="edit-info-icons" href="">
                    <img src="/public/images/ant-design_edit-outlined.svg" alt="">
                </a>
            </div>
        </div>
        <!--<div class="user-info-desc">
            <p class="user-info-description-title">Data rejestracji</p>
            <div class="user-info-value-cont">
                <p class="user-info-text"> 25.01.2022 </p>
            </div>
        </div>-->

        <div class="user-page-buttons">
            <button class="edit-info">Zmień dane</button>

        </div>

    </div>
</div>

<?php include __DIR__ . '/../components/footer.php'; ?>

</body>


</html>
